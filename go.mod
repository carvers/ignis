module gitlab.com/carvers/ignis

require (
	github.com/Kitt-AI/snowboy v1.3.0 // indirect
	github.com/brentnd/go-snowboy v0.0.0-20171119211911-bae46a0fd965
	github.com/go-audio/audio v0.0.0-20181013203223-7b2a6ca21480
	github.com/go-audio/wav v0.0.0-20181013172942-de841e69b884
	github.com/gordonklaus/portaudio v0.0.0-20180817120803-00e7307ccd93
	github.com/maxhawkins/go-webrtcvad v0.0.0-20160913155855-e9f766234143
	github.com/orcaman/writerseeker v0.0.0-20180723184025-774071c66cec
)
