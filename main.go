// This example streams the microphone thru Snowboy to listen for the hotword,
// by using the PortAudio interface.
//
// HOW TO USE:
// 	go run examples/Go/listen/main.go [path to snowboy resource file] [path to snowboy hotword file]
//
package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"time"

	snowboy "github.com/brentnd/go-snowboy"
	"github.com/go-audio/audio"
	"github.com/go-audio/wav"
	"github.com/gordonklaus/portaudio"
	webrtcvad "github.com/maxhawkins/go-webrtcvad"
	"github.com/orcaman/writerseeker"
)

// Sound represents a sound stream implementing the io.Reader interface
// that provides the microphone data.
type Sound struct {
	stream *portaudio.Stream
	data   []int16
}

// Init initializes the Sound's PortAudio stream.
func (s *Sound) Init() {
	inputChannels := 1
	outputChannels := 0
	sampleRate := 16000
	s.data = make([]int16, 1024)

	// initialize the audio recording interface
	err := portaudio.Initialize()
	if err != nil {
		fmt.Errorf("Error initialize audio interface: %s", err)
		return
	}

	// open the sound input stream for the microphone
	stream, err := portaudio.OpenDefaultStream(inputChannels, outputChannels, float64(sampleRate), len(s.data), s.data)
	if err != nil {
		fmt.Errorf("Error open default audio stream: %s", err)
		return
	}

	err = stream.Start()
	if err != nil {
		fmt.Errorf("Error on stream start: %s", err)
		return
	}

	s.stream = stream
}

// Close closes down the Sound's PortAudio connection.
func (s *Sound) Close() {
	s.stream.Close()
	portaudio.Terminate()
}

// Read is the Sound's implementation of the io.Reader interface.
func (s *Sound) Read(p []byte) (int, error) {
	s.stream.Read()

	buf := &bytes.Buffer{}
	for _, v := range s.data {
		binary.Write(buf, binary.LittleEndian, v)
	}

	copy(p, buf.Bytes())
	return len(p), nil
}

func main() {
	// open the mic
	mic := &Sound{}
	mic.Init()
	defer mic.Close()

	// open the snowboy detector
	d := snowboy.NewDetector(os.Args[1])
	defer d.Close()

	vad, err := webrtcvad.New()
	if err != nil {
		log.Fatal(err)
	}

	if err := vad.SetMode(2); err != nil {
		log.Fatal(err)
	}

	rate := 32000 // kHz
	frame := make([]byte, 320*2)

	if ok := vad.ValidRateAndFrameLength(rate, len(frame)); !ok {
		log.Fatal("invalid rate or frame length")
	}

	// set the handlers
	d.HandleFunc(snowboy.NewHotword(os.Args[2], 0.5), func(string) {
		fmt.Println("<heavy sigh> What?")
		start := time.Now()
		/*f, err := os.Create("/usr/local/var/www/static.carvers.house/ignis.wav")
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		*/
		var f writerseeker.WriterSeeker
		e := wav.NewEncoder(&f, 16000, 16, 1, 1)
		for {
			_, err := io.ReadFull(mic, frame)
			if err == io.EOF || err == io.ErrUnexpectedEOF {
				break
			}
			if err != nil {
				log.Fatal(err)
			}

			buf := &audio.PCMBuffer{
				Format: &audio.Format{
					NumChannels: 1,
					SampleRate:  16000,
				},
				I16:            mic.data,
				SourceBitDepth: 2,
				DataType:       audio.DataTypeI16,
			}
			err = e.Write(buf.AsIntBuffer())
			if err != nil {
				panic(err)
			}

			active, err := vad.Process(rate, frame)
			if err != nil {
				log.Fatal(err)
			}
			if !active {
				if time.Since(start) < time.Second {
					fmt.Println("Stopped talking in the first second, ignoring.")
					continue
				}
				fmt.Println("Stopped talking.")
				break
			}
			fmt.Println("still talking")
		}
		e.Close()
		f.Close()
		b, err := ioutil.ReadAll(f.Reader())
		if err != nil {
			log.Fatal(err)
		}
		err = ioutil.WriteFile("/usr/local/var/www/static.carvers.house/ignis.wav", b, 0777)
		if err != nil {
			log.Fatal(err)
		}
		h := sha256.New()
		h.Write(b)
		log.Println(hex.EncodeToString(h.Sum(nil)))
	})

	// start detecting using the microphone
	d.ReadAndDetect(mic)
}
